import React, { Component, Fragment } from 'react';
import logo from './logo.svg';
import './App.css';
import Guest from './Guest/Guest';
import Guests from './Guest';
import {Header, Footer} from './Layouts';
import guest from './Guest/Guest';

class App extends Component {

  // state = {
  //   guests: [
  //     {firstName : 'Leroy', lastName : 'Serepe', company: 'Google Inc', contact: 'Contact using number', email: 'leroy@google.co.za'},
  //     {firstName : 'James', lastName : 'Martins', company: 'Google Inc', contact: 'Contact using number', email: 'james@google.co.za'},
  //     {firstName : 'Sam', lastName : 'Stevens', company: 'Facebook Inc', contact: 'Contact using number', email: 'sam@facebook.co.za'}
  //   ]
  // };


  render() {
    return (
      <Fragment>
        <Header/>

        <Guests />
        
        <Footer/>
      </Fragment>
    );
  }
}

export default App;


// return

// <div className="App">
// {/* <header className="App-header">
//   <img src={logo} className="App-logo" alt="logo" />
//   <h1 className="App-title">Welcome to React</h1>
// </header>
// <p className="App-intro">
//   To get started, edit <code>src/App.js</code> and save to reload.
// </p> */}
// {guestList}
// {guests}

// </div>
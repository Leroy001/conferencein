import React, { Component} from 'react';
import {Grid, Paper, Typography, Avatar, Card, Input, CardMedia , CardContent, CardActions,
  Divider, Button, TextField, List,ListItemIcon } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles';
import GuestList from './GuestList';
import Guest from './Guest';
import parseString from 'xml2js';
import _ from 'lodash';
import parse from './parse';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Icon from '@material-ui/core/Icon';
import AddIcon from '@material-ui/icons/Add';
const styles = theme => ({
    Paper : { padding: 20, marginTop : 10, marginBottom: 10},
    card: {
        maxWidth: 400,
      },
      media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
      },
      actions: {
        display: 'flex',
      },
      expand: {
        transform: 'rotate(0deg)',
        transition: theme.transitions.create('transform', {
          duration: theme.transitions.duration.shortest,
        }),
        marginLeft: 'auto',
        [theme.breakpoints.up('sm')]: {
          marginRight: -8,
        },
      },
      expandOpen: {
        transform: 'rotate(180deg)',
      },
      avatar: {
        backgroundColor: '#fff',
        display: 'inline-flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontSize: '20px',
        borderRadius: '50%',
        height: '50px',
        width: '50px',
        top: '15px',
        left: '20px'
      },
      txtc : {
        color: '#000'
      },
      side :{
        color: '#2f2f2f'
      },
      button: {
        margin: theme.spacing.unit,
      }
})

class Guests extends Component {
  constructor (props){
    super(props);
    this.state = {
      items: [],
      isLoaded : false,
      guestTemp : {id : -1,
        first_name : 'Welcome',
        last_name: '',
        company : 'Please select a user from the list.',
        contancts : [],
        email : ''
    },
      guestJS : [],
     textvalue : '',
     filteredGuests : [],
     MobileNumbers: [],
     MobileNumberText: "",
     EmailErrorText: "",
     dataLoaded : false,

    }
  };
  checkEmailValue(e, type) {
    const value = e.target.value;
    const regexVal = type.regex.test(value) || false;
    if (!regexVal) {
      this.setState({EmailErrorText: "Enter a valid email eg: name@company.com"});
    } else {
        this.setState({EmailErrorText: ""});
    }
    if(type.isRequired && Object.keys(value).length === 0) {
      this.setState({EmailErrorText: "Enter your email address"});
    }
      const nextState = {};
      nextState[type] = value;
      this.setState(nextState);
      console.log(value);
  }

  addMoreMobileNumbers(){
    const MobileNumbers = this.state.MobileNumbers;
    if (MobileNumbers.length < 2) {
      this.setState({
          MobileNumbers: MobileNumbers.concat(
          <TextField

            key={this.state.MobileNumbers.length}
            fullWidth={true}
            label="Mobile Number"
            helperText={this.state.MobileNumberText}
            error={(this.state.MobileNumberText.length > 0)? this.state.MobileNumberText: ''}
            onChange={e => this.checkMobileValue(e,
                            {
                              type: 'MobileNumber',
                              regex: /^((?:\+27|27)|0)(\d{2})-?(\d{3})-?(\d{4})$/,
                              isRequired: true})}
            onBlur={this.state.isDisabled}/>
        )
      });
    }
  }
  componentDidMount(){
    fetch('https://gist.githubusercontent.com/eMatsiyana/e315b60a2930bb79e869b37a6ddf8ef1/raw/10c057b39a4dccbe39d3151be78c686dcd1101aa/guestlist.xml')//https://jsonplaceholder.typicode.com/users
      .then(res => res.text())
      .then(json => {
        this.setState({
          isLoaded: true,
          items : json
          //guestJS: parse(new DOMParser().parseFromString(this.state.items, "text/xml"))
        })
      });
  };

  componentDidUpdate(){
    if(!this.state.dataLoaded){
      this.addDataHandler();
      this.setState({
        dataLoaded : true
      })
    }
  }
  writeCookie(name, value) {
    var cookie = [name, '=', JSON.stringify(value), '; domain=.', window.location.host.toString(), '; path=/;'].join('');
    document.cookie = cookie;
  }

  readCookie(name) {
    var result = document.cookie.match(new RegExp(name + '=([^;]+)'));
    result && (result = JSON.parse(result[1]));
    return result;
   }


  addDataHandler = () =>{
    let mydata = parse(new DOMParser().parseFromString(this.state.items, "text/xml"));
    mydata = parse(new DOMParser().parseFromString(this.state.items, "text/xml"));

    let myGuestAr = [];

    for (var i = 0; i < mydata.length; i++){
        let guestObj = {};
        guestObj.id = i;
        guestObj.last_name =  mydata[i]['first_name'];
        guestObj.first_name =  mydata[i]['first_name'];
        guestObj.company =  mydata[i]['company'];
        guestObj.contancts =  [];
        guestObj.email =  '';
        myGuestAr.push(guestObj);
      }
    this.setState({guestJS: myGuestAr});
    // console.log(myGuestAr)
    myGuestAr = btoa(myGuestAr)

    this.writeCookie('myData', myGuestAr);
    this.readCookie('myData');
  }

  checkMobileValue(e, type) {
    const value = e.target.value;
    const regexVal = type.regex.test(value) || false;

    if (!regexVal) {
        this.setState({MobileNumberText: "Enter a valid South African mobile number eg +27812345678"});
    } else {
        this.setState({MobileNumberText: ""});
    }
    if(type.isRequired && Object.keys(value).length === 0) {
      this.setState({MobileNumberText: "Enter your mobile number"});
    }
      const nextState = {};
      nextState[type] = value;
      this.setState(nextState);debugger;
      console.log(value);
  }

  handleAddTodoItem(id) {
    /*this.state.value.push(this.state.textvalue)
    this.setState(
      this.state
    )
    this.state*/
    console.log(id)
  }

  handleChange=(e)=>{
    this.setState({
      textvalue:e.target.value
    })
  }

  loadData(){
    const mydata = parse(new DOMParser().parseFromString(this.state.items, "text/xml"));
    // this.setState({guestJS: mydata});
    return mydata;
  }

  searchGuest11 = (event) =>{
      let term = event.target.value.toLowerCase();
      let myGuestAr = [];

      for (var i=0; i < this.state.guestJS.length; i++) {
        let myGuestsSearch = this.state.guestJS;
        if (myGuestsSearch[i].first_name.indexOf(term) != -1) {
            myGuestAr.push(myGuestsSearch[i]);
        }else if(myGuestsSearch[i].last_name.indexOf(term) != -1 ){
          myGuestAr.push(myGuestsSearch[i]);
        }else if(myGuestsSearch[i].company.indexOf(term) != -1){
          myGuestAr.push(myGuestsSearch[i]);
        }
      }this.setState({filteredGuests: myGuestAr});
  }

  searchGuest = (event) =>{
    let term = event.target.value.toLowerCase();

    var myGuestAr=[];
    this.state.guestJS.forEach(function(guestObj){
      if(guestObj.first_name.toLowerCase().indexOf(term)!=-1)
      {
        myGuestAr.push(guestObj);
      }
      else if(guestObj.last_name.toLowerCase().indexOf(term)!=-1)
      {
        myGuestAr.push(guestObj);
      }
      else if(guestObj.company.toLowerCase().indexOf(term)!=-1)
      {
        myGuestAr.push(guestObj);
      }
    });
    this.setState({
      textvalue:term,
      filteredGuests: myGuestAr
    })
  }
  handleGuest = id =>{
    let tguest = {};
    if (this.state.filteredGuests.length > 0)
      tguest = this.state.filteredGuests[id];
    else
      tguest = this.state.guestJS[id];
    this.setState({guestTemp: tguest});
  }

  render() {
    let allguests;console.log('original: '+this.state.guestJS.length);
    console.log('filtered: '+this.state.filteredGuests.length);
    if (this.state.filteredGuests.length > 0) {
      allguests = Object.keys(this.state.filteredGuests).map(( index) =>
      <Paper key ={index}>
          {/* <Typography variant="headline" onClick={() => { this.handleGuest(index) }}>{this.state.guestJS[index]['first_name']} {this.state.guestJS[index]['last_name']}</Typography>             */}
          <ListItem
            key={index}
            primary={this.state.filteredGuests[index]['first_name'] + " " + this.state.filteredGuests[index]['last_name']} 
            onClick={() =>{this.handleGuest(index)}}
            leftAvatar={<Avatar style={styles.avatar} />}>
            {/* <ListItemIcon>
              
            </ListItemIcon> */}
              <ListItemText primary={this.state.filteredGuests[index]['first_name'] + " " + this.state.filteredGuests[index]['last_name']} />
            </ListItem>
      </Paper>
    );
    }else{
    allguests = Object.keys(this.state.guestJS).map(( index) =>
      <Paper key ={index}>
          {/* <Typography variant="headline" onClick={() => { this.handleGuest(index) }}>{this.state.guestJS[index]['first_name']} {this.state.guestJS[index]['last_name']}</Typography>             */}
          <ListItem
            key={index}
            primary={this.state.guestJS[index]['first_name'] + " " + this.state.guestJS[index]['last_name']}
            onClick={() =>{this.handleGuest(index)}}
            inset = {true}
            leftAvatar={<Avatar style={styles.avatar} />}>
              <ListItemText primary={this.state.guestJS[index]['first_name'] + " " + this.state.guestJS[index]['last_name']} />
            </ListItem>
      </Paper>
      );
    }
        return (
            <Grid container sm={12} spacing={8}>
                <Grid item sm={3} style={{height: 600, overflowY: 'auto', backgroundColor: '#2f2f2f', color: '#fff'}} >
                  <List>
                      <Input className="input-border-none" hint="Search Guests" onChange={this.searchGuest} />
                      <ListItem><ListItemText style={{ color: '#FFFFFF' }} primary={(this.state.filteredGuests.length > 0 ? this.state.filteredGuests.length : this.state.guestJS.length) + ' Confirmed Guests'}/></ListItem>
                      {allguests}
                  </List>
                </Grid>

                <Grid item sm={9} style={{height: 600}}>
                    <Paper style={styles.Paper}>
                         <Card className={styles.card}>
                            <Divider />
                            <CardMedia
                            className={styles.media}
                            image="/static/images/cards/paella.jpg"
                            title="Contemplative Reptile"
                            />
                            <CardContent>
                                <Grid item sm={12}>
                                    <Avatar aria-label="Recipe" className={styles.avatar}>

                                        </Avatar>
                                        <Typography variant="headline" component="h2" align="center"> {this.state.guestTemp.first_name} {this.state.guestTemp.last_name}</Typography>
                                        <Typography variant="subheading" style={{marginTop: 20, fontStyle: 'italic' }} fontStyle align="center">{this.state.guestTemp.company}</Typography>
                                </Grid>
                            <Divider />
                            <Grid justify="center">

                            </Grid>
                            <Typography variant="headline" component="h4">Contact Details:</Typography>
                            </CardContent>
                            <CardActions className={styles.actions} disableActionSpacing>
                            <Grid sm={6}>
                              <TextField
                                type="email"
                                fullWidth={true}
                                label="Email Address"
                                helperText={this.state.EmailErrorText}
                                error={(this.state.EmailErrorText.length > 0)? this.state.EmailErrorText: ''}
                                onChange={e => this.checkEmailValue(e,
                                                {
                                                  type: 'EmailAddress',
                                                  regex: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                                                  isRequired: true})}
                                onBlur={this.isDisabled}/><Divider />
                            </Grid>
                            <Grid sm={6}>
                              <TextField
                                
                                style={{color: '#000'}}
                                fullWidth={true}
                                label="Mobile Number"
                                helperText={this.state.MobileNumberText}
                                error={(this.state.MobileNumberText.length > 0)? this.state.MobileNumberText: ''}
                                onChange={e => this.checkMobileValue(e,
                                                { type: 'MobileNumber', regex: /^((?:\+27|27)|0)(\d{2})-?(\d{3})-?(\d{4})$/,
                                                  isRequired: true})}
                                onBlur={this.isDisabled}/><Divider />
                                {this.state.MobileNumbers.map((MobileNumber, index) => {
                                    return MobileNumber
                                })}
                            </Grid>
                            <Grid sm={6}>
                              <label className="add-number">
                                <Button
                                  backgroundColor="black"
                                  disabledColor="white"
                                  mini={true}
                                  onClick={this.addMoreMobileNumbers.bind(this)}><AddIcon />
                                </Button >
                                Add Number
                              </label>
                            </Grid>
                            </CardActions>
                        </Card>
                    </Paper>
                </Grid>
                {/* <Grid>
                <Button onClick={this.addDataHandler}>Add Data</Button>
                </Grid> */}
            </Grid>
        );
    }
}

export default Guests;
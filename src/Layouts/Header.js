import React from 'react'
import {AppBar, Toolbar, Typography} from '@material-ui/core';

export default props =>
    <AppBar position="static" style={{marginBottom: 5}}>
        <Toolbar>
          <Typography variant="headline" color="inherit">
              Conference In
          </Typography>
        </Toolbar>
      </AppBar>